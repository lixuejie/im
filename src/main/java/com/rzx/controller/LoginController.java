package com.rzx.controller;

import javax.servlet.http.HttpSession;

import com.rzx.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
public class LoginController {
    @Autowired
    LoginService loginService;

    @RequestMapping("/validate")
    public String validate(@RequestParam("username") String username, @RequestParam("password") String pwd, HttpSession httpSession) {
        if (username == null)
            return "login";
        String realpwd = loginService.getPwdByName(username);
        if (realpwd != null && pwd.equals(realpwd)) {
            long uid = loginService.getUidByName(username);
            httpSession.setAttribute("username", username);
            httpSession.setAttribute("uid", uid);
            return "chatroom";
        } else
            return "fail";
    }

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping("/logout")
    public String logout(HttpSession httpSession) {
        httpSession.removeAttribute("username");
        httpSession.removeAttribute("uid");
        return "login";
    }
}
