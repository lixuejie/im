package com.rzx.po;

import java.util.Date;

public class Message {

    public static final Long ONLINE = 0L;

    public static final Long BROADCAST = -1L;

    public static final Long OFFLINE = -2L;

    private Long id;
    //发送者
    private Long from;
    //发送者名称
    private String fromName;
    //接收者
    private Long to;
    //发送的文本
    private String text;
    //发送日期
    private Date date;

    private MessageType type = MessageType.TEXT;

    private String url;

    private boolean reverse;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFrom() {
        return from;
    }

    public void setFrom(Long from) {
        this.from = from;
    }

    public Long getTo() {
        return to;
    }

    public void setTo(Long to) {
        this.to = to;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public boolean isReverse() {
        return reverse;
    }

    public void setReverse(boolean reverse) {
        this.reverse = reverse;
    }

    public MessageType getType() {
        return type;
    }

    public void setType(MessageType type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
