package com.rzx.mapper;


import com.rzx.po.Message;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MessageMapper {
	List<Message> getMessages(Message message);
	void insert(Message message);

	Message select(@Param("id") Long id);

	Message selectByUrl(@Param("url") String url);
}
