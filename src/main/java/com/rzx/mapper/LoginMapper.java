package com.rzx.mapper;


import com.rzx.po.Staff;

public interface LoginMapper {
    Staff getPwdByName(String name);

    Staff getNameById(long id);
}
