package com.rzx.service;


import com.rzx.mapper.LoginMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.rzx.po.Staff;

@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.DEFAULT, timeout = 5)
@Service
public class LoginService {
    @Autowired
    LoginMapper loginMapper;

    public String getPwdByName(String name) {
        Staff s = loginMapper.getPwdByName(name);
        if (s != null)
            return s.getPassword();
        else
            return null;
    }

    public Long getUidByName(String name) {
        Staff s = loginMapper.getPwdByName(name);
        if (s != null)
            return (long) s.getStaffId();
        else
            return null;
    }

    public String getNameById(long id) {
        Staff s = loginMapper.getNameById(id);
        if (s != null)
            return s.getUserName();
        else
            return null;
    }


}
